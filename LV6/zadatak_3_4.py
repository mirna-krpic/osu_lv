import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

def plot_decision_regions(X, y, classifier, ax, resolution=0.02): # method altered to draw subplots
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    ax.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    ax.set_xlim(xx1.min(), xx1.max())
    ax.set_ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        ax.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("LV6\Social_Network_Ads.csv")
print(data.info())

data.hist()
plt.show()

# dataframe u numpy
X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# find best kernel
pipe = Pipeline([('scaler', StandardScaler()), ('svc', svm.SVC())])
param_grid = {'svc__kernel': ['linear', 'poly', 'rbf', 'sigmoid']}
grid_search = GridSearchCV(pipe, param_grid, cv=5, scoring ='accuracy', n_jobs =-1)
grid_search.fit(X_train, y_train)
best_kernel = grid_search.best_params_['svc__kernel']
print("Best kernel:", best_kernel)

# find best C and gamma parameters for best kernel
param_grid = {'C': [1, 10, 100 ],
              'gamma': [10, 1, 0.1, 0.01 ]}

fig, axes = plt.subplots(nrows=len(param_grid['C']), ncols=len(param_grid['gamma']), figsize=(15, 12))
for i, C in enumerate(param_grid['C']):
    for j, gamma in enumerate(param_grid['gamma']):
        SVM_model = svm.SVC(kernel = best_kernel, gamma=gamma, C=C)
        SVM_model.fit(X_train_n, y_train)
        plot_decision_regions(X_train_n, y_train, classifier=SVM_model, ax=axes[i,j])
        axes[i,j].set_title(f'C={C}, gamma={gamma}')
plt.show()

svm_gscv = GridSearchCV(SVM_model, param_grid, cv=5, scoring ='accuracy', n_jobs =-1)
svm_gscv.fit( X_train_n , y_train )
print(svm_gscv.best_params_)
