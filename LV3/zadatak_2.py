import pandas as pd
import matplotlib.pyplot as plt

def main():
    data = pd.read_csv('LV3\data_C02_emission.csv')
    podz_a(data)
    podz_b(data)
    podz_c(data)
    podz_d(data)
    podz_e(data)

def podz_a(data: pd.DataFrame):
    plt.figure()
    data['CO2 Emissions (g/km)'].plot(kind='hist')
    plt.title('CO2 Emissions')
    plt.show()

def podz_b(data: pd.DataFrame):
    fuels = data['Fuel Type'].unique()
    colors = {'X': 'r', 'Z': 'g', 'D': 'b', 'E': 'm', 'N': 'y'}
    point_colors = [colors[fuels] for fuels in data['Fuel Type']]
    data.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', cmap ="hot", s=5, c=point_colors)
    plt.show()

def podz_c(data: pd.DataFrame):
    data.boxplot(column =['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
    plt.show()

def podz_d(data: pd.DataFrame):
    fuels = data.groupby('Fuel Type')
    fuels['Make'].count().plot(kind='bar')
    plt.ylabel('Number of vehicles')
    plt.show()

def podz_e(data: pd.DataFrame):
    cilinders = data.groupby('Cylinders')
    cilinders['CO2 Emissions (g/km)'].mean().plot(kind='bar')
    plt.ylabel('CO2 emissions')
    plt.show()

if __name__ == '__main__':
    main()