import pandas as pd
import numpy as np

# a)
data = pd.read_csv('LV3\data_C02_emission.csv')

print(f'size: {len(data)}')

print(data.dtypes)
print (data.isnull().sum())
data.drop_duplicates()
data = data.reset_index(drop=True)
print(f'size after drop: {len(data)}')

object_cols = data.select_dtypes(include=['object']).columns
data[object_cols] = data[object_cols].astype('category')
print(data.dtypes)

# b)
top_city_consumption = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=True)
bottom_city_consumption = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False)
print(top_city_consumption[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print(bottom_city_consumption[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))

# c)
engine_size = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f'engine size: {len(engine_size)}')
print(engine_size['Engine Size (L)'].mean())

# d)
audi = data[(data['Make'] == 'Audi')]
print(f'audi: {len(audi)}')
print(audi[audi['Cylinders'] == 4]['CO2 Emissions (g/km)'].mean())

# e)
cyl = data.groupby(['Cylinders'])['CO2 Emissions (g/km)'].mean()
print(cyl)

# f)
# D - dizel
# X - benzin
benzin = data[(data['Fuel Type'] == 'X')]
dizel = data[(data['Fuel Type'] == 'D')]
print(f"benzin: {benzin['Fuel Consumption City (L/100km)'].mean()} median: {benzin['Fuel Consumption City (L/100km)'].median()}")
print(f"dizel: {dizel['Fuel Consumption City (L/100km)'].mean()} median: {dizel['Fuel Consumption City (L/100km)'].median()}")

# g)
the_vehicle = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'X')]
print(the_vehicle.sort_values(by='Fuel Consumption City (L/100km)', ascending=False).head(1))

# h)
manual = data[(data['Transmission'].str.contains('M'))]
print(len(manual))

# i)
print(data.corr(numeric_only=True))


# od 2 do 6 redka, od 2 do 7 stupca
# data.iloc[2:6, 2:7]