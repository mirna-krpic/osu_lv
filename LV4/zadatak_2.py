import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.metrics import *

data: pd.DataFrame = pd.read_csv('LV3/data_C02_emission.csv', delimiter=',')

x_data = ['Engine Size (L)',
          'Cylinders',
          'Fuel Type',
          'Fuel Consumption City (L/100km)',
          'Fuel Consumption Hwy (L/100km)',
          'Fuel Consumption Comb (L/100km)',
          'Fuel Consumption Comb (mpg)']
X = data[x_data]
y = data['CO2 Emissions (g/km)']

ohe = OneHotEncoder(sparse=False)
transformed = ohe.fit_transform(X[['Fuel Type']])
X_encoded = pd.DataFrame(transformed, columns=ohe.get_feature_names_out())
X = pd.concat([X, X_encoded], axis=1)
X = X.drop(['Fuel Type'], axis=1)
print(X.head(5))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)

linearModel = lm.LinearRegression()
linearModel.fit( X_train, y_train )

y_test_p = linearModel.predict( X_test )

x_data.remove('Fuel Type')
for label in x_data:
    plt.scatter(X_test[label], y_test, c='blue', s=1, label='Real data')
    plt.scatter(X_test[label], y_test_p, c='red', s=1, label='Predicted data')
    plt.xlabel(label)
    plt.ylabel('CO2 Emissions (g/km)')
    plt.legend()
    plt.show()

errors = np.abs(y_test - y_test_p)
max_error_index = np.argmax(errors)
max_error_model = data.iloc[max_error_index, 1]
print(max_error_model)
