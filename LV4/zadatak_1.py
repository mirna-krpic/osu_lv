import math
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.metrics import *

data: pd.DataFrame = pd.read_csv('LV3/data_C02_emission.csv', delimiter=',')

x_data = ['Engine Size (L)',
          'Cylinders',
          'Fuel Consumption City (L/100km)',
          'Fuel Consumption Hwy (L/100km)',
          'Fuel Consumption Comb (L/100km)',
          'Fuel Consumption Comb (mpg)']
X = data[x_data]
y = data['CO2 Emissions (g/km)']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)

plt.scatter(X_train['Fuel Consumption Hwy (L/100km)'], y_train, s=2, c='b', label='Training data')
plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test, s=2, c='r', label='Test data')
plt.ylabel('CO2 Emissions (g/km)')
plt.xlabel('Fuel Consumption Hwy (L/100km)')
plt.legend()
plt.show()

sc = MinMaxScaler()
X_train_n = sc.fit_transform( X_train )
X_test_n = sc.transform( X_test )
X_train_n_df = pd.DataFrame(X_train_n, columns=X.columns)

fig, axes = plt.subplots(2, 1)
X_train.hist('Fuel Consumption Hwy (L/100km)', ax=axes[0], color='r')
X_train_n_df.hist('Fuel Consumption Hwy (L/100km)', ax=axes[1])
axes[0].set_title('Original')
axes[1].set_title('Scaled')
plt.show()

linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )

y_test_p = linearModel.predict( X_test_n )

plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test,  s=1, c='r', label='Real data')
plt.scatter(X_test['Fuel Consumption Hwy (L/100km)'], y_test_p,  s=1, c='g', label='Predicted data')
plt.xlabel('Fuel Consumption Hwy (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()

MAE = mean_absolute_error( y_test , y_test_p )
MSE = mean_squared_error( y_test, y_test_p )
RMSE = math.sqrt( MSE )
R2 = r2_score( y_test, y_test_p )
MAPE = mean_absolute_percentage_error( y_test, y_test_p )

print(f"Model 80-20:\nMAE: {MAE}\nMSE: {MSE}\nRMSE: {RMSE}\nR2: {R2}\nMAPE: {MAPE}")

# g) mijenjanje veličine skupova
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 1)

X_train_n = sc.fit_transform( X_train )
X_test_n = sc.transform( X_test )
X_train_n_df = pd.DataFrame(X_train_n, columns=X.columns)

linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )

y_test_p = linearModel.predict( X_test_n )

MAE = mean_absolute_error( y_test , y_test_p )
MSE = mean_squared_error( y_test, y_test_p )
RMSE = math.sqrt( MSE )
R2 = r2_score( y_test, y_test_p )
MAPE = mean_absolute_percentage_error( y_test, y_test_p )

print(f"\nModel 70-30:\nMAE: {MAE}\nMSE: {MSE}\nRMSE: {RMSE}\nR2: {R2}\nMAPE: {MAPE}")