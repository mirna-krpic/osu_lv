import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("LV2/road.jpg")
img = img[ :,:,0].copy ()

#a)
plt.figure()
plt.imshow(img, 'gray', alpha=0.5)
plt.show()

#b)
rows, cols = img.shape
imgQ = img[:, round(cols/4):round(cols/2), ].copy()
plt.figure()
plt.imshow(imgQ, 'gray')
plt.show()

#c)
rotateImg = np.rot90(img, axes=(0,1))
plt.figure()
plt.imshow(rotateImg, 'gray')
plt.show()

#d)
fliped = np.fliplr(img)
plt.figure()
plt.imshow(fliped, 'gray')
plt.show()