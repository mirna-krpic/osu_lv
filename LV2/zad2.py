import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('LV2/data.csv', delimiter=",", skiprows=1)
print(data)

#a)
print(data.shape[0])

#b)
height = data[:, 1]
weight = data[:, 2]
plt.scatter(height, weight,  s=0.5)
plt.xlabel('height')
plt.ylabel('weight')
plt.show()

#c)
height1 = data[0::50, 1]
weight1 = data[0::50, 2]
plt.scatter(height1, weight1, s=0.5)
plt.xlabel('height')
plt.ylabel('weight')
plt.show()

#d)
print(np.mean(height))
print(height.max())
print(height.min())

#e)
m = (data[:,0] == 1)
z = (data[:,0] == 0)

print('m:')
print(np.mean(data[m,1]))
print(data[m,1].max())
print(data[m,1].min())

print('z:')
print(np.mean(data[z,1]))
print(data[z,1].max())
print(data[z,1].min())