import numpy as np
import matplotlib.pyplot as plt

black = np.zeros((50, 50))
white = np.ones((50, 50))
image = np.zeros((100,100))

image[50:, :] = np.hstack((white, black))
image[:, 50:] = np.vstack((white, black))

plt.imshow(image, cmap='gray')
plt.show()
