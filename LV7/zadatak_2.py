import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

def quantize_colors(img):
    fig, axes = plt.subplots(nrows=1, ncols=2)
    # prikazi originalnu sliku
    
    axes[0].set_title("Originalna slika")
    axes[0].imshow(img)

    # pretvori vrijednosti elemenata slike u raspon 0 do 1
    img = img.astype(np.float64) / 255

    # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
    w,h,d = img.shape
    img_array = np.reshape(img, (w*h, d))

    # rezultatna slika
    img_array_aprox = img_array.copy()

    km = KMeans(n_clusters=3, init ='k-means++', n_init=5, random_state=0)
    km.fit(img_array)

    labels = km.predict(img_array)

    for i in range(w*h):
        img_array_aprox[i,:] = km.cluster_centers_[labels[i],:]

    img_aprox = np.reshape(img_array_aprox, (w, h, d))

    axes[1].set_title("Rezultantna slika")
    axes[1].imshow(img_aprox)

    plt.tight_layout()
    plt.show()

    # https://towardsdatascience.com/clustering-how-to-find-hyperparameters-using-inertia-b0343c6fe819
    inertia_list = []
    for num_clusters in range(1, 11):
        km = KMeans(n_clusters=num_clusters, init="k-means++", n_init=5, random_state=0)
        km.fit(img_array)
        inertia_list.append(km.inertia_)
        
    # plot the inertia curve
    plt.plot(range(1,11),inertia_list)
    plt.scatter(range(1,11),inertia_list)
    plt.xlabel("Number of Clusters - K", size=13)
    plt.ylabel("Inertia Value - J", size=13)
    plt.title("Vrijednost J u ovisnosti o broju centara K")
    plt.show()
    

# ucitaj sliku
for i in range(1, 7):
    img = Image.imread(f'imgs\\test_{i}.jpg')
    quantize_colors(img=img)

    
